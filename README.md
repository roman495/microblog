# Flask Tutorial Project
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

## Env
- `export FLASK_APP=microblog.py`
- `export FLASK_ENV=development`
- `export FLASK_ENV=production`

## Mail
- `export MAIL_SERVER=sandbox.smtp.mailtrap.io`
- `export MAIL_PORT=2525`
- `export MAIL_USE_TLS=1`
- `export MAIL_USERNAME=7c6aa0331c9f89`
- `export MAIL_PASSWORD=0dbcdcd98cbf2a`

## Db
- `flask db init`
- `flask db migrate -m "users table"
- `flask db upgrade`