"""new fields in user model

Revision ID: 314050522162
Revises: ceb2d4a53b91
Create Date: 2018-02-06 08:48:16.703335

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '314050522162'
down_revision = 'ceb2d4a53b91'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('about_me', sa.String(length=140), nullable=True))
    op.add_column('user', sa.Column('last_seen', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'last_seen')
    op.drop_column('user', 'about_me')
    # ### end Alembic commands ###
